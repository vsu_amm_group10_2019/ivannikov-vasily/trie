﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    class Tree
    {
        public Node Root { get; set; } = new Node();
        public void Add(string value)
        {
            Root.Add(value.ToLower());
        }
        public void PrintTree(TreeView treeView)
        {
            if (Root != null)
            {
                treeView.Nodes.Add("");
                Root.PrintTree(treeView.Nodes[0]);
            }
        }
        public void Delete(string word)
        {
            if (Root != null)
            {
                Root.Delete(word.ToLower());
            }
        }
        public void DeleteLength(int length)
        {
            Root.DeleteLength(length);
        }
    }
}
