﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        private string FileName { get; set; }
        Tree tree = new Tree();
        public Form1()
        {
            InitializeComponent();
        }
        private void Refresh()
        {
            treeView.Nodes.Clear();
            tree.PrintTree(treeView);
            treeView.ExpandAll();
        }
        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openFileDialog.ShowDialog();
            string fileName = openFileDialog.FileName;
            string[] lines = File.ReadAllLines(fileName);
            foreach (string str in lines)
            {
                string[] words = str.Split(' ');
                foreach (string word in words)
                {
                    string tmp = word.Trim();
                    if (!string.IsNullOrEmpty(tmp))
                    {
                        tree.Add(word);
                    }
                }
            }
            Refresh();
        }

        private void addToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Text readForm = new Text();
            readForm.ShowDialog();
            if (readForm.flag)
            {
                tree.Add(readForm.text);
                Refresh();
            }
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Text readForm = new Text();
            readForm.ShowDialog();
            if (readForm.flag)
            {
                tree.Delete(readForm.text);
                Refresh();
            }
        }

        private void deleteLengthToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Digit d = new Digit();
            d.ShowDialog();
            tree.DeleteLength(d.digit);
            Refresh();
        }
    }
}
