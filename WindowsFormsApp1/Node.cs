﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public class Node
    {
        public Dictionary<char, Node> Children { get; set; } = new Dictionary<char, Node>();
        public int Count { get; set; }
        public void Add(string value)
        {
            if (value.Length == 0)
            {
                Count++;
                return;
            }
            char next = value[0];
            string remaining = value.Length == 1 ? "" : value.Substring(1);

            if (!Children.ContainsKey(next))
            {
                Node node = new Node();
                Children.Add(next, node);
            }

            Children[next].Add(remaining);
        }
        public bool Find(string value)
        {
            if (value.Length == 0)
                return Count > 0;

            char next = value[0];
            string remaining = value.Length == 1 ? "" : value.Substring(1);

            if (!Children.ContainsKey(next))
                return false;

            return Children[next].Find(remaining);
        }
        public void PrintTree(TreeNode treeNode)
        {
            int i = 0;
            foreach (var kv in Children)
            {
                treeNode.Nodes.Add(kv.Key.ToString());
                kv.Value.PrintTree(treeNode.Nodes[i]);
                i++;
            }
        }
        public bool Delete(string word)
        {
            if (word == "")
            {
                if (Children.Count != 0)
                {
                    Count = 0;
                    return false;
                }
                return true;
            }
            else
            {
                char next = word[0];
                string remaining = word.Length == 1 ? "" : word.Substring(1);

                if (Children.ContainsKey(next))
                {
                    if (Children[next].Delete(remaining))
                    {
                        Children.Remove(next);
                        if (Children.Count > 0)
                            return false;

                        return true;
                    }
                }
            }
            return false;
        }
        public int Check(ref int digit, KeyValuePair<char, Node> key, int length)
        {
            foreach (var k in Children)
            {
                digit++;
                int d = digit;
                if (k.Value.Children.Count > 1)
                {
                    k.Value.Check(ref digit, key, length);
                }
                else
                {
                    k.Value.Check(ref digit, key, length);
                }
                digit = d;

                if (digit > length)
                    key.Value.Children.Clear();
                break;
            }
            return digit;
        }
        public void DeleteLength(int length)
        {
            char[] del = new char[Children.Count];
            int index = 0;
            foreach (var keyValuePair in Children)
            {
                int i = 1;
                if (keyValuePair.Value.Check(ref i, keyValuePair, length) > 1)
                {
                    del[index] = keyValuePair.Key;
                    index++;
                }
            }
            foreach (char c in del)
                Delete(c.ToString());
        }
    }
}
